# Cucumber 2022

Als je gebruik maakt van Intellij, activeer dan de Gherkin en Cucumber
plugins. Activeer ook de git en maven plugins, mochten die nog niet aanstaan.
Bij andere IDE's zul je waarschijnlijk ook in ieder geval een Java, Git en
Cucumber plugin willen gebruiken.

Er zijn ongeveer 10 commits met opdrachten. Als je vastloopt op een opdracht,
dan kun je spieken bij de volgende commit, en je kunt natuurlijk ook aan ons
een vraag stellen. Als je er gewoon niet uitkomt, ga dan door naar de
volgende commit, want de tijd vliegt zo voorbij. Je kunt op een later moment
altijd nog eens kijken hoe het precies zit.

De opdrachten maken verwijzingen naar StaQ. StaQ is een conferentie die door
en voor Quintor medewerkers georganiseerd wordt. Deze workshop is in eerste
instantie voor StaQ 2022 gemaakt en is aangepast voor vandaag. Als er in een
opdracht naar (de dag van) StaQ verwezen wordt, ga er dan vanuit dat vandaag
(15-11-2022) de dag is waarop StaQ plaats vindt.

De opdrachten van de workshop staan in de commit messages. In de opvolgende
commits staan de uitwerkingen zoals wij die gemaakt hebben. Dus als in commit
1 in de commit message een opdracht staat, dan staat de uitwerking van die
opdracht in commit 2. En in commit 2 staat in de commit message de volgende
opdracht. In intellij kun je met alt-9 het git-venster openen. In de log-tab
van het git-venster zie je de git history. Je kunt hiermee door de commits
navigeren (commit messages lezen, onze oplossingen even inzien met diff, etc).
De eerste commit die je wil uitchecken is 'Test dat de staq-checker kan checken
dat het tijd is voor StaQ' (rechtermuis op de commit > checkout revision xxx).

Je kunt met 'git reset --hard' werk dat niet is gecommit verwijderen en weer
terugkeren naar de beginstaat van een commit. Je kunt met 'git stash -u'
werk dat niet is gecommit in de stash opslaan. Als je direct naar de
volgende commit wil zonder iets op te slaan, dan kun je eenvoudigweg die
commit uitchecken.

Onderaan de commit messages staan soms referenties naar websites. Neem
gerust de tijd om daar doorheen te bladeren.

1) Check de commit 'Beginstaat van de workshop' uit.

Referenties:

https://www.atlassian.com/git/tutorials/undoing-changes/git-reset
https://www.atlassian.com/git/tutorials/saving-changes/git-stash