package applicatie;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StaqChecker {
    private final Klok klok;

    private boolean isHetTijdVoorStaQ;
    private List<LocalDate> startData;

    public StaqChecker(Klok klok) {
        this.klok = klok;
        isHetTijdVoorStaQ = getIshetTijdVoorStaq();
        startData = Collections.emptyList();
    }

    public boolean getIshetTijdVoorStaq() {
        return isHetTijdVoorStaQ;
    }

    public void updateIsHetTijdVoorStaq() {
        isHetTijdVoorStaQ = LocalDate.of(2022, 11, 15).equals(klok.vandaag());
    }

    public void updateStartdata(List<LocalDate> data) {
        startData = data;
    }

    public int dagenTotStaq() {
        return dagenTotStaqVanafDatum(klok.vandaag());
    }

    public List<Integer> dagenTotStaqVanafData() {
        return startData.stream()
                .map(this::dagenTotStaqVanafDatum)
                .collect(Collectors.toList());
    }

    private int dagenTotStaqVanafDatum(LocalDate datum) {
        return LocalDate.of(2022, 11, 15).compareTo(datum);
    }
}
