package applicatie;

import java.time.LocalDate;
import java.util.function.Supplier;

public class Klok {
    private Supplier<LocalDate> vandaagSupplier;

    public Klok() {
        vandaagSupplier = LocalDate::now;
    }

    public void setVandaagSupplier(Supplier<LocalDate> vandaagSupplier) {
        this.vandaagSupplier = vandaagSupplier;
    }

    public LocalDate vandaag() {
        return vandaagSupplier.get();
    }
}
