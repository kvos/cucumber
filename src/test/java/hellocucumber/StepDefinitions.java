package hellocucumber;

import applicatie.Klok;
import applicatie.StaqChecker;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;

import io.cucumber.java.ParameterType;
import io.cucumber.java.nl.Als;
import io.cucumber.java.nl.Dan;
import io.cucumber.java.nl.Stel;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StepDefinitions {
    public Klok klok;
    public StaqChecker staqChecker;

    @Before
    public void nieuweStaqChecker() {
        klok = new Klok();
        staqChecker = new StaqChecker(klok);
    }

    @Stel("vandaag is {datum}")
    public void vandaagIsDatum(LocalDate datum) {
        klok.setVandaagSupplier(() -> datum);
    }

    @Als("ik de 'is het tijd voor staq' update")
    public void updateIsHetTijdVoorStaq() {
        staqChecker.updateIsHetTijdVoorStaq();
    }

    @Als("ik de startdata update met")
    public void updateStartdata(List<LocalDate> startdata) {
        staqChecker.updateStartdata(startdata);
    }

    @Dan("^dan is het (wel|niet) tijd voor StaQ$")
    public void welOfNietTijdVoorStaq(String welOfNiet) {
        if (welOfNiet.equals("wel")) {
            assertTrue(staqChecker.getIshetTijdVoorStaq());
        } else {
            assertFalse(staqChecker.getIshetTijdVoorStaq());
        }
    }

    @Dan("dan is het {int} dagen tot StaQ")
    public void dagenTotStaq(int dagen) {
        assertEquals(dagen, staqChecker.dagenTotStaq());
    }

    @Dan("is het aantal dagen tot StaQ vanaf de startdata")
    public void dagenTotStaqVanafStartdata(List<Integer> dagenVanafStartData) {
        assertEquals(dagenVanafStartData, staqChecker.dagenTotStaqVanafData());
    }

    @ParameterType("([0-9]{2})-([0-9]{2})-([0-9]{4})")
    public LocalDate datum(String dag, String maand, String jaar) {
        int dagGetal = Integer.parseInt(dag);
        int maandGetal = Integer.parseInt(maand);
        int jaarGetal = Integer.parseInt(jaar);
        return LocalDate.of(jaarGetal, maandGetal, dagGetal);
    }

    @DataTableType
    public LocalDate localDateTransformer(String datum) {
        String[] datumElementen = datum.split("-");
        int dagGetal = Integer.parseInt(datumElementen[0]);
        int maandGetal = Integer.parseInt(datumElementen[1]);
        int jaarGetal = Integer.parseInt(datumElementen[2]);
        return LocalDate.of(jaarGetal, maandGetal, dagGetal);
    }
}
