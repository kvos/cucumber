# language: nl
Functionaliteit: : Is het tijd voor StaQ?
  Iedereen wil weten of het tijd voor StaQ is

  Abstract Scenario: het is wel of niet tijd voor StaQ
    Stel vandaag is <datum>
    Als ik de 'is het tijd voor staq' update
    Dan dan is het <wel of niet> tijd voor StaQ

    Voorbeelden:
      | datum      | wel of niet |
      | 14-11-2022 | niet        |
      | 15-11-2022 | wel         |

  Abstract Scenario: hoeveel dagen tot StaQ
    Stel vandaag is <datum>
    Als ik de 'is het tijd voor staq' update
    Dan dan is het <aantal dagen> dagen tot StaQ

    Voorbeelden:
      | datum      | aantal dagen |
      | 14-11-2022 | 1            |
      | 15-11-2022 | 0            |


  Scenario: hoeveel dagen tot StaQ vanaf startdata
    Als ik de startdata update met
      | 14-11-2022 |
      | 15-11-2022 |
    Dan is het aantal dagen tot StaQ vanaf de startdata
      | 1 |
      | 0 |